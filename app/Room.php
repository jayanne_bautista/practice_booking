<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Room
 *
 * @package App
 * @property string $room_number
 * @property integer $floor
 * @property text $description
 */
class Room extends Model
{
    use SoftDeletes;

    protected $fillable = ['room_number','room_name', 'floor', 'description', 'room_category', 'hasWifi', 'capacity', 'ventilation', 'price'];

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setFloorAttribute($input)
    {
        $this->attributes['floor'] = $input ? $input : null;
    }
    public function booking()//needs to change to bookings
    {
        return $this->HasMany(Booking::class, 'room_id')->withTrashed();
    }
    public function gallery(){
        return $this->HasMany(Gallery::class);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    //
  
    protected $guarded = [];
    protected $table = 'gallery';
    public function room(){
        return $this->belongsTo('Room', 'room_id');
    }

}

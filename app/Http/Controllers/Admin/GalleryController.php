<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
 	public function deleteImage($id)
    {
        
        $image = Gallery::findOrFail($id);
        Storage::delete($image->filename);
        $image->delete();


        return response()->json(true);
        
    }    
 	  
}

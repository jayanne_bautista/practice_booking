<?php

namespace App\Http\Controllers\Admin;

use App\Room;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRoomsRequest;
use App\Http\Requests\Admin\UpdateRoomsRequest;
use Illuminate\Support\Facades\Storage;

class RoomsController extends Controller
{
    /**
     * Display a listing of Room.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('room_access')) {
            return abort(401);
        }


        if (request('show_deleted') == 1) {
            if (! Gate::allows('room_delete')) {
                return abort(401);
            }
            $rooms = Room::onlyTrashed()->get();
        } else {
            $rooms = Room::all();
        }

        return view('admin.rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating new Room.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (! Gate::allows('room_create')) {
            return abort(401);
        }
        $filesize = ini_get('upload_max_filesize');
        return view('admin.rooms.create', compact('filesize'));
    }

    /**
     * Store a newly created Room in storage.
     *
     * @param  \App\Http\Requests\StoreRoomsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoomsRequest $request)
    {
        if (! Gate::allows('room_create')) {
            return abort(401);
        }
        $gallery = $request->gallery;
       
        switch ($request->room_category) {
            case '0':
                $request->room_category = 'single';
                break;
            case '1':
                $request->room_category = 'double';
                break;
            case '2':
                $request->room_category = 'family';
                break;
            default:
                # code...
                break;
        }
       
        $room = Room::create($request->all());
        if($gallery){
            foreach ($gallery as $photo) {

                $file = $photo->store('public/gallery');
                $filename = substr(strstr($file, "/"), 1);

                Gallery::create([
                    'room_id' => $room->id,
                    'filename' => $filename
                ]);
            }

        }
        
        
        


        return redirect()->route('admin.rooms.index');
    }


    /**
     * Show the form for editing Room.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('room_edit')) {
            return abort(401);
        }
        $room = Room::findOrFail($id);


        return view('admin.rooms.edit', compact('room'));
    }

    /**
     * Update Room in storage.
     *
     * @param  \App\Http\Requests\UpdateRoomsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoomsRequest $request, $id)
    {
        if (! Gate::allows('room_edit')) {
            return abort(401);
        }
        $room = Room::findOrFail($id);
        switch ($request->room_category) {
            case '0':
                $request->room_category = 'single';
                break;
            case '1':
                $request->room_category = 'double';
                break;
            case '2':
                $request->room_category = 'family';
                break;
            default:
                # code...
                break;
        }

        $room->update($request->all());

        $gallery = $request->gallery;
       
        if($gallery){
            foreach ($gallery as $photo) {

                $file = $photo->store('public/gallery');
                $filename = substr(strstr($file, "/"), 1);

                Gallery::create([
                    'room_id' => $room->id,
                    'filename' => $filename
                ]);
            }
        }
        

        return redirect()->route('admin.rooms.index');
    }


    /**
     * Display Room.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (! Gate::allows('room_view')) {
            return abort(401);
        }
        $bookings = \App\Booking::where('room_id', $id)->get();

        $room = Room::findOrFail($id);

        return view('admin.rooms.show', compact('room', 'bookings'));
    }


    /**
     * Remove Room from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (! Gate::allows('room_delete')) {
            return abort(401);
        }
        $room = Room::findOrFail($id);
        $room->delete();

        return redirect()->route('admin.rooms.index');
    }

    /**
     * Delete all selected Room at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if (! Gate::allows('room_delete')) {
            return abort(401);
        }
        if ($request->input('ids')) {
            $entries = Room::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }


    /**
     * Restore Room from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        if (! Gate::allows('room_delete')) {
            return abort(401);
        }
        $room = Room::onlyTrashed()->findOrFail($id);
        $room->restore();

        return redirect()->route('admin.rooms.index');
    }

    /**
     * Permanently delete Room from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function perma_del($id)
    {
        if (! Gate::allows('room_delete')) {
            return abort(401);
        }
        $room = Room::onlyTrashed()->findOrFail($id);
        $room->forceDelete();

        return redirect()->route('admin.rooms.index');
    }

    public function deleteImage($id)
    {
        
        // $image = Gallery::findOrFail($id);
        // Storage::delete($image->filename);
        // $image->delete();


        // return response()->json(true);
        return response()->json(1);
    } 
}

<?php

namespace App\Http\Controllers\Website;

use App\Booking;
use App\Customer;
use App\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ReservationEmail;
use App\Notifications\ResevationMessage;
use Mail;
use Nexmo;


class BookingsController extends Controller
{
	 public function reserve(Request $request){
        //get request
        $customer = Customer::where('email',$request->email)->first();
        $time_from = $request->time_from;
	    $time_to = $request->time_to;
	    $additional_information = isset($request->additional_information)?$request->additional_information:' ';
	    $room_id = $request->room_id;
	   
        if($customer){
        	 $customer->phone = $request->phone;
        	$customer->update();
        }
        else{
        	$first_name = $request->first_name;
	        $last_name = $request->last_name;
	        $email = $request->email;
	        $phone = $request->phone;
	       
	        $address = $request->address;
	       
	        $customer = Customer::create([
	        	'first_name'=>$first_name,
	        	'last_name'=>$last_name,
	        	'email'=>$email,
	        	'phone'=>$phone,
	        	'address'=>$address

	        ]);

        }
        //check if booked
        $time_from = new Carbon($time_from);
        $time_to = new Carbon($time_to);
        $booked = Booking::where('room_id', $room_id)
        	->whereDate('time_from', '<=', $time_from )
            ->whereDate('time_to', '>=', $time_to)
			->first(); 

	  
        if($booked){
        	return redirect()->back()->with('error', 'Not available.');
        }
        else{
        	$booking = new Booking();
		    $booking->unique_code();
		    $booking->time_from =  $time_from;
	        $booking->customer_id = $customer->id;
	        $booking->time_to =$time_to;
	        $booking->room_id = $request->room_id;

			$booking->additional_information = $request->additional_information;
	        $booking->save();
	        $room = $booking->room;
	        $length = $time_to->diffInDays($time_from);
	        $price = $length * $room->price;

	        $data = [
	        	'code' => $booking->code,
	        	'price'=>$price,
	        	'time_from'=>$time_from->toFormattedDateString(),
	        	'time_to'=>$time_to->toFormattedDateString(),
	        	'room'=>$room->name,
	        	'customer_name'=>$customer->first_name,



	        ];

    		Mail::to($customer->email)->send(new ReservationEmail($data));
    		
			$text = 'You successfully reserved room'.$room->room_name.'for dates '. $time_from.'to'.$time_to.' Verification Code :'.$booking->code.' Price:'.$price;
		
			
			$text = 'code:'.$booking->code.' #:'.$room->id.' ';
			try{
				Nexmo::message()->send([
				    'to'   => $customer->phone,
				    'from' => '639755635432',
				    'text' => $text
			    

				]);
			}
			catch(Exception $e){
				return redirect()->back()->with('message', 'successfully added reservation but not able to send text');
			}
    		
        	return redirect()->back()->with('message', 'Successfully added reservation');
      

        }
       
        

      	
        //save customer
        //save bookings

    }
   
    //
}

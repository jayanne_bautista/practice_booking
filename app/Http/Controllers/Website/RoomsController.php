<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Room;
use App\Gallery;
use App\Booking;
use App\Customer;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DB;
class RoomsController extends Controller
{
    //
    /**
     * Display a listing of Room.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $rooms = Room::all();

        $single = Room::where('room_category', '=', 'single')->get();

        $double = Room::where('room_category', '=', 'double')->get();
        $family = Room::where('room_category', '=', 'family')->get();

        return view('website.rooms', compact('rooms','single', 'double', 'family'));
    }
    public function home()
    {
        $rooms = Room::paginate(2);

        $single = Room::where('room_category', '=', 'single')->paginate(3);

        $double = Room::where('room_category', '=', 'double')->paginate(3);
        $family = Room::where('room_category', '=', 'family')->paginate(3);
    
        return view('website.home', compact('rooms', 'single', 'double', 'family'));

    }
    



    /**
     * Display Room.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $room = Room::findOrFail($id);
        $time_from = date('Y-m-d');
        $time_to = date('Y-m-d');
        $dates =array();
        $period = [];

        foreach ($room->booking as $key => $value) {
            if($value->status != 'declined'){
                $period = CarbonPeriod::create($value->time_from, $value->time_to);
               foreach ($period as $date) {
                    array_push($dates, $date->format('Y-m-d')) ;
                }
            }
           

        }
      
       
        return view('website.room', compact('room', 'time_from', 'time_to', 'dates'));
    }
    public function searchRoom(Request $request){
        $time_from = new Carbon($request->date_from);


        $time_to = new Carbon($request->date_to);
        $room_category = $request->room_category;

        if ($request->isMethod('POST')) {
            


            $rooms = Room::with('booking')

                ->whereNOTIn('id',function($query) use ($time_to, $time_from){
                     $query->select('room_id')->from('bookings')
                    ->whereDate('time_from', '<', $time_to)
                    ->whereDate('time_to', '>', $time_from)
                    ->where('status', '!=', 'declined');
            })->orWhereDoesntHave('booking')
            ->get();
         

        } else {
            $rooms = null;
        }

        
        $rooms = $rooms->where('room_category', $room_category);

        
        
        return view('website.search', compact('rooms', 'time_from', 'time_to', 'room_category'));
    }
    public function showResultRoom($id, $time_from, $time_to)
    {

        
        $room = Room::findOrFail($id);
        $dates =array();
        $period = [];

        foreach ($room->booking as $key => $value) {
            if($value->status != 'declined'){
                $period = CarbonPeriod::create($value->time_from, $value->time_to);
               foreach ($period as $date) {
                    array_push($dates, $date->format('Y-m-d')) ;
                }
            }
           

        }
        

        return view('website.room', compact('room', 'time_from', 'time_to', 'dates'));
    }

   

}

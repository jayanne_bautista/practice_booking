<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReservationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'uahotelreservation@mailinator.com';
        $subject = 'Hotel Reservation';
        $name = 'Urban Inn';
        
        return $this->view('emails.reservation')
                    ->from($address, $name)
                    // ->cc($address, $name)
                    // ->bcc($address, $name)
                    // ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([
                        'price'=>$this->data['price'],
                        'code'=>$this->data['code'],
                        'time_from'=>$this->data['time_from'],
                        'time_to'=>$this->data['time_to'],
                        'room'=>$this->data['room'],
                        'customer_name'=>$this->data['customer_name']
                    ]);
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Create1516728105RoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if(! Schema::hasTable('rooms')) {
            Schema::create('rooms', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('floor');
                $table->integer('room_number');
                $table->string('room_name');
                $table->text('description')->nullable();
                $table->boolean('hasWifi');
                $table->enum('ventilation', ['fan', 'ac']);
                $table->integer('capacity');
                
                $table->decimal('price', 8, 2);
                $table->enum('room_category', ['single', 'double', 'family']);
                $table->timestamps();
                $table->softDeletes();
                $table->index(['deleted_at']);
            });
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // if(! Schema::hasTable('gallery')) {
            Schema::create('gallery', function (Blueprint $table) {
                $table->increments('id');
                if (!Schema::hasColumn('gallery', 'room_id')) {
                    $table->integer('room_id')->unsigned()->nullable();
                    $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade');
                }
                $table->string('filename');
                 $table->timestamps();
            });
        // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
       Schema::dropIfExists('gallery');
    }
}

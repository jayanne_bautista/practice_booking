@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.rooms.title')</h3>
    
    {!! Form::model($room, ['method' => 'PUT', 'route' => ['admin.rooms.update', $room->id], 'files'=>true]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('room_number', trans('quickadmin.rooms.fields.room-number').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('room_number', old('room_number'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('room_number'))
                        <p class="help-block">
                            {{ $errors->first('room_number') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('floor', trans('quickadmin.rooms.fields.floor').'*', ['class' => 'control-label']) !!}
                    {!! Form::number('floor', old('floor'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('floor'))
                        <p class="help-block">
                            {{ $errors->first('floor') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('Name', trans('Name').'*', ['class' => 'control-label']) !!}
                    {!! Form::text('room_name', old('room_name'), ['class' => 'form-control ', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('room_name'))
                        <p class="help-block">
                            {{ $errors->first('room_name') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('room_category', trans('Category').'*', ['class' => 'control-label']) !!}
                    {!! Form::select('room_category', ['single'=>'single', 'double' => 'double', 'family'=>'family'], old('room_category'), ['class' => 'form-control select2']) !!}
                    <p class="help-block"></p>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('description', trans('quickadmin.rooms.fields.description').'*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control ', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('description'))
                        <p class="help-block">
                            {{ $errors->first('description') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('price', trans('Price per Night').':', ['class' => 'control-label']) !!}
                    {!! Form::number('price', old('price'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('ventilation', trans('Ventilation').':', ['class' => 'control-label']) !!}
                    {!! Form::label('ac', trans('AC').'', ['class' => 'control-label']) !!}
                    {!! Form::radio('ventilation', 'ac', old('ventilation')) !!}
                    {!! Form::label('fan', trans('Fan').'', ['class' => 'control-label']) !!}
                     {!! Form::radio('ventilation', 'fan', old('ventilation')) !!}
                    <p class="help-block"></p>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('hasWifi', trans('Has Wifi').':', ['class' => 'control-label']) !!}
                     {!! Form::label('hasWifiYes', trans('Yes').':', ['class' => 'control-label']) !!}

                     {!! Form::radio('hasWifi', 1 , false) !!}
                      {!! Form::label('hasWifiNo', trans('No').':', ['class' => 'control-label']) !!}
                     {!! Form::radio('hasWifi', 0 , true) !!}

                    <p class="help-block"></p>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('capacity', trans('Capacity').':', ['class' => 'control-label']) !!}
                    {!! Form::number('capacity', old('capacity'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    
                </div>
            </div>

            <div class="row">
                 <input type="file" name="gallery[]" id="file" multiple>

                
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12 ">
                    @if(count($room->gallery))
                        @foreach($room->gallery as $image)
                            <div class="col-xs-2">
                                <input  type="hidden" name="csrf-token" content="{{ csrf_token() }}>

                                <input type="hidden" value='{{route("admin.delete.image", $image->id)}}' id="delete-route"/>
                                <img src='{{url("storage/".$image->filename)}}' class="room-thumbnail">
                                <a class="remove-image" data-image-id = '{{$image->id}}'><span class="glyphicon glyphicon-remove"></span></a>   
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            
        </div>
        
    </div>
@stop


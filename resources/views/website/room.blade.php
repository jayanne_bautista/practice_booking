@extends('layouts.website')

@section('content')


  <section class="probootstrap-slider flexslider">
    <ul class="slides">
      @if(count($room->gallery))
        @foreach($room->gallery as $image)
         <li style="background-image: url(/storage/{{$image->filename}});" class="overlay">
            <div class="container">
              <div class="row">
                <div class="col-md-10 col-md-offset-1">
                  <div class="probootstrap-slider-text text-center">
                    
                    
                  </div>
                </div>
              </div>
            </div>
          </li>
        @endforeach
      @endif
    </ul>
  </section>
 
  <section class="probootstrap-section ">
    
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    @if(session()->has('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div>
    @endif
    <div class="container">
      <div class="row probootstrap-gutter60 ">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-12 purple-bg">
              <h2 class="mt0">{{$room->room_name}} - Php {{$room->price}}</h2>
              <p><img src="/atlantis/img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
              <div class="row">
                <div class="col-md-12">
                  <p>{{$room->description}}</p>    
                </div>
                
              </div>
              <h2 class="mt0">Amenities</h2>
              <p><img src="/atlantis/img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
              <div class="row">
                <div class="col-md-12">

                  @if($room->hasWifi)
                    <p class="facilities-text"><i class="icon-check"></i> Free Wifi </p>
                  @endif  
                  @if($room->ventilation == 'fan')
                    <p class="facilities-text"><i class="icon-check"></i>Fan   </p>
                  @endif
                  @if($room->ventilation == 'ac')
                    <p class="facilities-text"> <i class="icon-check"></i>Airconditioned </p>
                  @endif
                  <p  class="facilities-text"><i class="icon-check"></i>{{$room->capacity}} person/s</i></p>
                </div>
                
              </div>
              
            </div>

          </div>
        </div>
          
      </div>
        
        
      </div>
   
    
  </section>
  <section class="probootstrap-cta">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="probootstrap-cta-heading">Reserve this for your family <span> &mdash;.</span></h2>
          <div class="probootstrap-cta-button-wrap"><a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Reserve now</a></div>
        </div>
      </div>
    </div>
  </section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="exampleModalLabel">Reserve This Room</h4>
        
      </div>
      <div class="modal-body">
        <form action="{{route('reserve')}}" method="post"  id="form-reservation-modal" class="probootstrap-form">
          {{ csrf_field() }}
            <input type="hidden" name="room_id" value="{{$room->id}}">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="first_name">First Name</label>
                  <input type="text" class="form-control" id="first_name" name="first_name" required="">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="last_name">Last Name</label>
                  <input type="text" class="form-control" id="last_name" name="last_name" required="">
                </div>
              </div>
            </div>
            <div class="form-group">
              <label for="email">Email</label>
              <div class="form-field">
                <i class="icon icon-mail"></i>
                <input type="email" class="form-control" id="email" name="email" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="phone">Phone Number</label>
              <div class="form-field">
                <i class="icon icon-phone"></i>
                <input type="tel" class="form-control" id="phone" name="phone" required="">
              </div>
            </div>
            <div class="form-group">
              <label for="address">Address</label>
              <div class="form-field">
                
                <input type="text" name="address" class="form-control">
              </div>
            </div>
           
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="date-arrival">From</label>
                  <div class="form-field">
                    <i class="icon icon-calendar2"></i>
                    <input type="text" class="form-control datetimepicker" id="time_from_reserve" name="time_from" value="{{$time_from}}">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="date-departure">To</label>
                  <div class="form-field">
                    <i class="icon icon-calendar2"></i>
                    <input type="text" class="form-control datetimepicker" id="time_to_reserve" name="time_to" value="{{$time_to}}">
                  </div>
                </div>
              </div>
            </div>
            
            <div class="form-group">
              <label for="additional_information">Additional Information</label>
              <div class="form-field">
                
                <textarea class="form-control" id="additional_information" name="additional_information"></textarea>
              </div>
            </div>
           
            <div class="form-group">
              <input type="submit" class="btn btn-primary btn-lg" id="submit" name="submit" value="Reserve">
            </div>
          </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div> -->
    </div>
  </div>
</div>
<input type="hidden" name="dates" id="room_dates" value='<?php echo json_encode($dates); ?>'>

<hr>


@stop

@extends('layouts.website')
<style type="text/css">
.room-info{
  background: rgba(0, 0, 0, 0.5);
  width:100%;
  left: 0!important;
  right: 0!important;
}
.carousel-inner>.item>a>img, .carousel-inner>.item>img{
  width: 100%;
}
</style>

@section('content')
  <section class="probootstrap-slider flexslider">
    <ul class="slides">
       <li style="background-image: url(atlantis/img/urban-inn.webp);" class="overlay">
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="probootstrap-slider-text text-center">
                  <p><img src="atlantis/img/curve_white.svg" class="seperator probootstrap-animate" alt="Free HTML5 Bootstrap Template"></p>
                  <h1 class="probootstrap-heading probootstrap-animate">Welcome to Urban Inn</h1>

                  <!-- <div class="probootstrap-animate probootstrap-sub-wrap">Insert Lines Here</div> -->
                </div>
              </div>
            </div>
          </div>
        </li>
        
          
        </li>
    </ul>
  </section>

  <section class="probootstrap-cta">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2 class="probootstrap-cta-heading"><span> </span></h2>
           <div class="row">
            
              @include('partials.website.search-form')
            </div>
          


          
        </div>
      </div>
    </div>
  </section>

 
  <section class="probootstrap-section">
    <div class="container">
      <div class="row mb30">
        <div class="col-md-8 col-md-offset-2 probootstrap-section-heading text-center">
          <h2>Rooms</h2>
          <p class="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
          <p><img src="atlantis/img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        </div>
      </div>
      <div class="row probootstrap-gutter10" >
        <h3>Single Rooms</h3>
        <p><img src="atlantis/img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        <div id="singleRoomCarousel" class="carousel slide" data-ride="carousel">
        
          <ol class="carousel-indicators">
              @foreach($single as $key => $room)
                <li data-target="#singleRoomCarousel" data-slide-to="{{$key}}" class="{{($key) == 0 ? 'active' : ''}}"></li>
                
                @endforeach
          </ol>   
          
          <div class="carousel-inner">
            @foreach($single as $key => $room)
              <div class="item {{($key) == 0 ? 'active' : ''}}">
                  @if(count($room->gallery))
                    <img src='{{ url("storage/".$room->gallery[0]["filename"]) }}' >
                    @else
                   <img src='/atlantis/img/img_7.jpg' alt="no image available">
                 @endif
              <div class="carousel-caption room-info" >
                <p>{{$room->room_name}}</p>
               
                <p>{{$room->description}}</p>
                <p><a href="{{route('room', $room->id)}}" class="btn btn-primary">Book now from {{$room->price}}</a></p>
              </div>
              </div>
               
            @endforeach
            
          </div>  
           
          <a class="carousel-control left" href="#singleRoomCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="carousel-control right" href="#singleRoomCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>
    <div class="row probootstrap-gutter10" >
        <h3>Double Rooms</h3>
        <p><img src="atlantis/img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        <div id="doubleRoomCarousel" class="carousel slide" data-ride="carousel">
        
          <ol class="carousel-indicators">
              @foreach($double as $key => $room)
                <li data-target="#doubleRoomCarousel" data-slide-to="{{$key}}" class="{{($key) == 0 ? 'active' : ''}}"></li>
                
              @endforeach
          </ol>   
          
          <div class="carousel-inner">
            @foreach($double as $key => $room)
              <div class="item {{($key) == 0 ? 'active' : ''}}">
                  @if(count($room->gallery))
                    <img src='{{ url("storage/".$room->gallery[0]["filename"]) }}' >
                    @else
                    <img src='/atlantis/img/img_7.jpg' alt="no image available">
                 @endif
              <div class="carousel-caption room-info" >
                <p>{{$room->room_name}}</p>
               
                <p>{{$room->description}}</p>
                <p><a href="{{route('room', $room->id)}}" class="btn btn-primary">Book now from {{$room->price}}</a></p>
              </div>
              </div>
               
            @endforeach
            
          </div>  
           
          <a class="carousel-control left" href="#doubleRoomCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="carousel-control right" href="#doubleRoomCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>
      <div class="row probootstrap-gutter10" >
        <h3>Family Rooms</h3>
        <p><img src="atlantis/img/curve.svg" class="svg" alt="Free HTML5 Bootstrap Template"></p>
        <div id="familyRoomCarousel" class="carousel slide" data-ride="carousel">
           
            <ol class="carousel-indicators">
              @foreach($family as $key => $room)
                <li data-target="#familyRoomCarousel" data-slide-to="{{$key}}" class="{{($key) == 0 ? 'active' : ''}}"></li>
                
                @endforeach
            </ol>   
         
          
          <div class="carousel-inner">
            @foreach($family as $key => $room)
              <div class="item {{($key) == 0 ? 'active' : ''}}">
                  @if(count($room->gallery))
                    <img src='{{ url("storage/".$room->gallery[0]["filename"]) }}' >
                    @else
                    <img src='/atlantis/img/img_7.jpg' alt="no image available">
                 @endif
              <div class="carousel-caption room-info" >
                <p>{{$room->room_name}}</p>
               
                <p>{{$room->description}}</p>
                <p><a href="{{route('room', $room->id)}}" class="btn btn-primary">Book now from {{$room->price}}</a></p>
              </div>
              </div>
               
            @endforeach
            
          </div>  
           
          <a class="carousel-control left" href="#familyRoomCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="carousel-control right" href="#familyRoomCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
        </div>
      </div>
    </div>
  </section>
  

  <section class="probootstrap-half">
    <div class="image" style="background-image: url(atlantis/img/slider_2.jpg);"></div>
    <div class="text">
      <div class="probootstrap-animate fadeInUp probootstrap-animated">
        <h2 class="mt0">Best Budget Inn in Town</h2>
        <p><img src="atlantis/img/curve_white.svg" class="seperator" alt="Free HTML5 Bootstrap Template"></p>
        <div class="row">
          <div class="col-md-6">
           
          </div>
          <div class="col-md-6">
              
          </div>
        </div>
        
      </div>
    </div>
  </section>
 
@stop

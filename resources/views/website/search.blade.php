@extends('layouts.website')

@section('content')
	<hr>
	<section class="probootstrap-cta">
		<div class="container">
			<div class="row">
				<form method="post" action = "{{route('search-room')}}" id="form-search">
   					{{ csrf_field() }}
				    <div class="col-md-3 col-xs-12 form-group">
				        <!-- <label for="time_from">From</label> -->
				        <input class="form-control datetimepicker" placeholder="From" required="" name="date_from" value="{{$time_from}}"  type="text" id="date_from2">
				        <p class="help-block"></p>
				    </div>
				    <div class="col-md-3 col-xs-12 form-group">
				        <!-- <label for="time_to">To</label> -->
				        <input class="form-control datetimepicker" placeholder="To" required="" name="date_to"  value="{{$time_to}}"  type="text" id="date_to2">
				        <p class="help-block"></p>
				    </div>
				    <div class="col-md-3 col-xs-12form-group">
				        <select class="form-control" name="room_category" id="room_category">
				          <option value="single" {{($room_category == 'single') ? 'selected' : ''}}>Single Room</option>
				          <option value="double" {{($room_category == 'double') ? 'selected' : ''}}>Double Room</option>
				          <option value="family" {{($room_category == 'family') ? 'selected' : ''}}>Family Room</option>
				        </select>
				    </div>
				    <div class="col-md-3 col-xs-12 form-group">

				         <input type="submit"  id="search-btn" name="" value="Search Room">
				    </div>
				</form>
			</div>

			
		</div>
		
	</section>
	<section class="probootstrap-cta">
		<div class="container">
			<div class="row">
				@if(count($rooms))

					@foreach($rooms as $room)
						<div class="col-md-12 room-results">
							 @if(count($room->gallery))
							 	<div class="col-md-4 col-xs-12">
							 		  <a href="{{route('room-result', ['id' => $room->id, 'time_from'=>$time_from, 'time_to'=>$time_to])}}"><img src='{{ url("storage/".$room->gallery[0]["filename"]) }}' alt="" class="img-responsive small-image" ></a>
							 	</div>
					            
					         @endif
					         	<div class="col-md-4 col-xs-12">
							 		
							 		 <h3>{{$room['room_name']}}</h3>
					                <p>Starting from <strong>Php {{$room['price']}}/Night</strong></p>
					                <p>{{ str_limit($room['description'], 20)}}</p>
							 	</div>
							 	<div class="col-md-4 col-xs-12">
							 		<br>
							 		<p><a href="{{route('room-result', ['id' => $room->id, 'time_from'=>$time_from, 'time_to'=>$time_to])}}" class="btn btn-primary"> Go to room</a></p>
							 	</div>
					             
					  	</div>              
					                
					               
					@endforeach
				@else
					<div class="col-md-12">
						<p>No available rooms.</p>						
					</div>
				@endif

			
			
			
		</div>
		
	</section>
	
@stop
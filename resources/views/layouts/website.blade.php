<!DOCTYPE html>
<html lang="en">

<head>
    @include('partials.website.head')
</head>


<body>
    @include('partials.website.header')
    @yield('content')
   
    @include('partials.website.footer')
    @include('partials.website.javascript')
</body>
</html>
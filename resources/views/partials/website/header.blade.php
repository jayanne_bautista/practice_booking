<header role="banner" class="probootstrap-header">
    <!-- <div class="container"> -->
    <div class="row">
        <a href="index.html" class="probootstrap-logo visible-xs"><img src="{{ url('atlantis/img') }}/logo_sm.png" class="hires" width="120" height="33" alt="Free Bootstrap Template by uicookies.com"></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs"><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
          <ul class="probootstrap-main-nav">
            
            <li class="{{(Request::is('/')?'active':'')}}"><a href="/">Home</a></li>
            
            <li class="hidden-xs">
              <a href="/" class="logo-text">
                URBAN INN
              </a>
            </li>
            <li class="{{(Request::is('rooms')?'active':'')}}"><a href="/rooms">Rooms</a></li>
            <!-- <li class="{{(Request::is('reservation')?'active':'')}}"><a href="{{url('reservation')}}">Reservation</a></li> -->
            <!-- <li class="{{(Request::is('contact')?'active':'')}}"><a href="/contact">Contact</a></li> -->
          </ul>
          <div class="extra-text visible-xs">
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Connect With Us</h5>
            <ul class="social-buttons">
              <li><a href="#"><i class="icon-twitter"></i></a></li>
              <li><a href="#"><i class="icon-facebook2"></i></a></li>
              <li><a href="#"><i class="icon-instagram2"></i></a></li>
            </ul>
          </div>
        </nav>
      </div>
    <!-- </div> -->
</header>
<!-- START: footer -->
  <footer role="contentinfo" class="probootstrap-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="probootstrap-footer-widget">
            <p class="mt40 logo-text">URBAN INN</p>
            <p>Urban Inn is a budget hotel in Iloilo</p>
            
          </div>
        </div>
        
        <div class="col-md-6">
          <div class="probootstrap-footer-widget">
            <h3>Contact</h3>
            <ul class="probootstrap-contact-info">
             
              <li><i class="icon-location2"></i> <span>Iloilo</span></li>
              <li><i class="icon-mail"></i><span>urbaninniloilo@gmail.com/span></li>
              <li><i class="icon-phone2"></i><span>Landline:  (033) 323 4277</span></li>
              <li><i class="icon-phone2"></i><span>Mobile: 0908 874 9976, 0917 529 2754</span></li>
            </ul>
          
          </div>
        </div>
      </div>
      <div class="row mt40">
        <div class="col-md-12 text-center">
          <ul class="probootstrap-footer-social">
            <li><a href=""><i class="icon-twitter"></i></a></li>
            <li><a href=""><i class="icon-facebook"></i></a></li>
            <li><a href=""><i class="icon-instagram2"></i></a></li>
          </ul>
          <!-- <p>
            <small>&copy; 2017 <a href="https://uicookies.com/" target="_blank">uiCookies:Atlantis</a>. All Rights Reserved. <br> Designed &amp; Developed by <a href="https://uicookies.com/" target="_blank">uicookies.com</a> Demo Images: Unsplash.com &amp; Pexels.com</small>
          </p> -->
          
        </div>
      </div>
    </div>
  </footer>
  <script src="{{ url('atlantis/js') }}/scripts.min.js"></script>
  <script src="{{ url('atlantis/js') }}/main.min.js"></script>
  <script src="{{ url('atlantis/js') }}/custom.js"></script>
  
    <script src="{{ url('atlantis/js') }}/moment.min.js"></script>
    <script src="{{ url('atlantis/js') }}/bootstrap-datetimepicker.min.js"></script>
    <script>
        var dates = [];
        var obj = $("#room_dates").val();
        
       
        if(obj){
          dates = JSON.parse(obj);
        }
        
        if(dates.length > 0){
          dates.map(function($date){
            return new Date($date);
          });
        }
        console.log(dates);
        $('.datetimepicker').datetimepicker({
          format: "YYYY-MM-DD",
          disabledDates: dates,
          minDate:new Date()
        });
       
        
      $("#form-reservation").on('submit', function(){
        var date_to = new Date($("#date_to").val());
        var date_from = new Date($("#date_from").val());

        var isValid = false;
        if(date_from >= new Date() ){
          if(date_to > date_from){
            isValid = true;
          }
        }
        if(!isValid){
          alert('Please Enter Valid Dates.');
        }
        
        
        return isValid;

      });
      $("#form-search").on('submit', function(){
        var date_to = new Date($("#date_to2").val());
        var date_from = new Date($("#date_from2").val());

        var isValid = false;
        if(date_from >= new Date() ){
          if(date_to > date_from){
            isValid = true;
          }
        }
        if(!isValid){
          alert('Please Enter Valid Dates.');
        }
        
        
        return isValid;

      });
      $("#form-reservation-modal").on('submit', function(){
        var date_to = new Date($("#time_to_reserve").val());
        var date_from = new Date($("#time_from_reserve").val());

        var isValid = false;
        if(date_from >= new Date() ){
          if(date_to > date_from){
            isValid = true;
          }
        }
        if(!isValid){
          alert('Please Enter Valid Dates.');
        }
        
        
        return isValid;

      });
   
  
    </script>
    <script src="https://www.gstatic.com/firebasejs/5.3.0/firebase.js"></script>

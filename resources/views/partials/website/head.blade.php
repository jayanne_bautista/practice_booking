<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hotel Reservation</title>
    <meta name="description" content="Free Bootstrap Theme by uicookies.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <link href="https://fonts.googleapis.com/css?family=Crimson+Text:300,400,700|Rubik:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('atlantis/css') }}/styles-merged.css">
    <link rel="stylesheet" href="{{ url('atlantis/css') }}/style.min.css">
    <link rel="stylesheet" href="{{ url('atlantis/css') }}/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="{{ url('atlantis/css') }}/custom.css">
    <style type="text/css">
    	td.day.disabled{
    		background: #ffc4c4 !important;
    	}
    </style>
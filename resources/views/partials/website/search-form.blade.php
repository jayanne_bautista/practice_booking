<form method="post" action = "{{route('search-room')}}" id="form-reservation">
   {{ csrf_field() }}
    <div class="col-md-3 col-xs-12 form-group">
        <!-- <label for="time_from">From</label> -->
        <input class="form-control datetimepicker" placeholder="From" required="" name="date_from"  type="text" id="date_from">
        <p class="help-block"></p>
    </div>
    <div class="col-md-3 col-xs-12 form-group">
        <!-- <label for="time_to">To</label> -->
        <input class="form-control datetimepicker" placeholder="To" required="" name="date_to"   type="text" id="date_to">
        <p class="help-block"></p>
    </div>
    <div class="col-md-3 col-xs-12form-group">
        <select class="form-control" name="room_category" id="room_category">
          <option value="single">Single Room</option>
          <option value="double">Double Room</option>
          <option value="family">Family Room</option>
        </select>
    </div>
    <div class="col-md-3 col-xs-12 form-group">

         <input type="submit"  id="search-btn" name="" value="Search Room">
    </div>
</form>
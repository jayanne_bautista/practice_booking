<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<h1>Hi {{$customer_name}}</h1>
	<p>You successfully reserved room {{$room}} for dates <strong>{{$time_from}}</strong> to <strong>{{$time_to}}</strong></p>
	<p>Just go to the reception and present verification code</p>
	<p>Price to pay : <strong>Php {{$price}}</strong></p>
	<p>Verification Code : <strong>{{$code}}</strong></p>

</body>
</html>